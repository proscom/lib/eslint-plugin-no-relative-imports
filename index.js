const path = require("path");
const { minimatch } = require('minimatch');

function isParentFolder(relativePath, context, rootDir, minLevel) {
  const relativePathPrefix = '../'.repeat(minLevel + 1);

  const absoluteRootPath = context.getCwd() + (rootDir !== '' ? path.sep + rootDir : '');
  const absoluteFilePath = path.join(path.dirname(context.getFilename()), relativePath)

  return relativePath.startsWith(relativePathPrefix) && (
    rootDir === '' ||
    (absoluteFilePath.startsWith(absoluteRootPath) &&
      context.getFilename().startsWith(absoluteRootPath))
  );
}

function getAbsolutePath(relativePath, context, rootDir) {
  return path
    .relative(
      context.getCwd() + (rootDir !== '' ? path.sep + rootDir : ''),
      path.join(path.dirname(context.getFilename()), relativePath)
    )
    .split(path.sep)
    .join("/");
}

function isSameFolder(path) {
  return path.startsWith("./");
}

function isPathIgnored(path, pathsToIgnore) {
  if (!pathsToIgnore || !pathsToIgnore.length) return false;
  return pathsToIgnore.some(pattern => minimatch(path, pattern));
}

function isPathAllowed(path, pathsToAllow) {
  if (!pathsToAllow || !pathsToAllow.length) return true;
  return pathsToAllow.some(pattern => minimatch(path, pattern));
}

const message = "import statements should have an absolute path";

module.exports = {
  rules: {
    "no-relative-import-paths": {
      meta: {
        type: "layout",
        fixable: "code",
      },
      create: function (context) {
        const { allowSameFolder, rootDir, minLevel, ignorePaths, allowPaths } = {
          allowSameFolder: context.options[0]?.allowSameFolder || false,
          rootDir: context.options[0]?.rootDir || '',
          minLevel: context.options[0]?.minLevel || 0,
          ignorePaths: context.options[0]?.ignorePaths || [],
          allowPaths: context.options[0]?.allowPaths || []
        };

        return {
          ImportDeclaration: function (node) {
            const path = node.source.value;

            const absolutePath = getAbsolutePath(path, context, rootDir || '');

            if (isPathIgnored(absolutePath, ignorePaths) || !isPathAllowed(absolutePath, allowPaths)) return;

            if (!allowSameFolder && isSameFolder(path)) {
              context.report({
                node,
                message: message,
                fix: function (fixer) {
                  return fixer.replaceTextRange(
                    [node.source.range[0] + 1, node.source.range[1] - 1],
                    absolutePath
                  );
                },
              });
            }

            if (isParentFolder(path, context, rootDir, minLevel)) {
              context.report({
                node,
                message: message,
                fix: function (fixer) {
                  return fixer.replaceTextRange(
                    [node.source.range[0] + 1, node.source.range[1] - 1],
                    absolutePath
                  );
                },
              });
            }
          },
        };
      },
    },
  },
};
