Fork of [eslint-plugin-no-relative-import-paths](https://www.npmjs.com/package/eslint-plugin-no-relative-import-paths)

# eslint-plugin-no-relative-import-paths

Moving a file to different folder, could result in changing all imports statement in that file. This will not happen is the import paths are absolute. The eslint rule helps enforcing having absolute import paths.
Support eslint --fix to automatically change imports to absolute paths.  

# Installation

Install [ESLint](https://www.github.com/eslint/eslint) either locally or globally. (Note that locally, per project, is strongly preferred)

```sh
$ npm install eslint --save-dev
```

If you installed `ESLint` globally, you have to install this plugin globally too. Otherwise, install it locally.

```sh
$ npm install @proscom/eslint-plugin-no-relative-import-paths --save-dev
```

# Configuration

Add the plugin to the plugins section, and configure the rule options.

```json
{
  "plugins": ["@proscom/eslint-plugin-no-relative-import-paths"],
  "rules": {
    "@proscom/no-relative-import-paths/no-relative-import-paths": [
      "warn",
      {
        "allowSameFolder": true,
        ...
      }
    ]
  }
}
```

## Rule options

```json
...
"@proscom/no-relative-import-paths/no-relative-import-paths": [
  "warn",
  {
    "allowSameFolder": boolean,
    "rootDir": string,
    "minLevel": number,
    "ignorePaths": string[],
    "allowPaths": string[]
  }
]
...
```

- `enabled`: for enabling the rule. 0 = off, 1 = warn, 2 = error. Defaults to 0.

### `allowSameFolder`

When `true` the rule will ignore relative import paths for imported files from the same folder

Examples of code for this rule:

```js
// when true this will be ignored
// when false this will generate a warning
import Something from "./something";

// will always generate a warning
import Something from "../modules/something";
```

### `rootDir`

Useful when auto-fixing and the rootDir should not be included in the absolute path.

Examples of code for this rule:

```js
// when not configured:
import Something from "../../components/something";

// will result in
import Something from "src/components/something";
```

```js
// when configured as { "rootDir": "src" }
import Something from "../../components/something";

// will result in
import Something from "components/something";
//                     ^- no 'src/' prefix is added
```

### `ignorePaths`

Plugin receives `string[]` with `glob-pattern` paths and allows to ignore them from being converted by plugin.

Examples of code for this rule:

```js
/**
 * i.e. project struct is:
 *
 * src
 *  - /components
 *  - /styles
 *  
 * rootDir: 'src'
 * ignorePaths: [ 'components/*' ]
 */

// will generate a warning
import style from "../styles/style";
// won't generate a warning
import component from "../components/component";

// will be converted to
import style from "styles/style";
import component from "../components/component";
```

### `allowPaths`

Plugin receives `string[]` with `glob-pattern` paths and allows them to be converted by plugin.

Examples of code for this rule:

```js
/**
 * i.e. project struct is:
 *
 * src
 *  - /components
 *  - /styles
 *  
 * rootDir: 'src'
 * allowPaths: [ 'components/*' ]
 */

// will generate a warning
import component from "../components/component";
// won't generate a warning
import style from "../styles/style";

// will be converted to
import style from "../styles/style";
import component from "components/component";
```

### `minLevel (default: 0)`

Provides minimum non-warning relativity level (amount of `../` prefixes)

Examples of code for this rule:

```js
/**
 * minLevel: 1 => '../'
 * minLevel: 2 => '../../'
 * ..etc
 */

/**
 * i.e.
 * minLevel: 1
 */

// will generate a warning
import component from "../../components";
// won't generate a warning
import cfg from "../config";
```
